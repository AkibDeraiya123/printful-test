// Dependencies
import React, { Component } from 'react';
import {
    Row,
    Container,
    Form,
    Button,
    Alert
} from 'react-bootstrap';
import axios from 'axios';

// Configuration file
import config from '../config';
// Sub components
import Question from './questions';
// Local CSS
import './landing.css';

export default class Landing extends Component {
    constructor(props) {
        super(props);

        this.state = {
            testList: [],
            selectedTest: '',
            name: '',
            errorMessage: '',
            questions: []
        };
    }

    componentDidMount() {
        // Fetch list of available test
        axios.get(`${config.apiBaseURL}/test`)
            .then(response => {
                this.setState({ testList: response.data.result.test })
            })
            .catch(error => {
                this.setState({
                    errorMessage: error.response && error.response.data && error.response.data.message ? error.response.data.message : 'Something went wrong.'
                })
            })
    };

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    }

    submitForm = (event) => {
        event.preventDefault();

        if (this.state.name.trim() === '') {
            // Validation for name
            this.setState({
                errorMessage: 'Please enter name'
            });
        } else if (this.state.selectedTest.trim() === '') {
            // Validation for test selection
            this.setState({
                errorMessage: 'Please select test'
            })
        } else {
            // Submit for test
            axios
                .get(`${config.apiBaseURL}/questions/${this.state.selectedTest}`)
                .then(response => {
                    this.setState({ questions: response.data.result.questions })
                })
                .catch(error => {
                    this.setState({
                        errorMessage: error.response && error.response.data && error.response.data.message ? error.response.data.message : 'Something went wrong.'
                    });
                })
        }
    };

    ErrorMessage() {
        if (this.state.errorMessage !== "") {
            return (<Alert variant="danger">{this.state.errorMessage}</Alert>)
        }
    };

    render() {
        return (
            <React.Fragment>
                <Container className="main-container">
                    <Row className="h-100 justify-content-md-center justify-content-center align-items-center">
                        <div className="col-md-6 mainDiv">
                            {
                                !this.state.questions.length ?
                                    <Form onSubmit={this.submitForm}>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Name <code>*</code></Form.Label>
                                            <Form.Control value={this.state.name} onChange={this.handleChange} type="text" name="name" placeholder="Enter name" />
                                        </Form.Group>

                                        <Form.Group controlId="exampleForm.ControlSelect1">
                                            <Form.Label>Example select <code>*</code></Form.Label>
                                            <Form.Control as="select" value={this.state.selectedTest} name="selectedTest" onChange={this.handleChange}>
                                                <option value="">-- Select test -- </option>
                                                {
                                                    this.state.testList.map((e, key) => {
                                                        return (<option key={key} value={e.id}>{e.name}</option>);
                                                    })
                                                }
                                            </Form.Control>
                                        </Form.Group>

                                        <Form.Group className="text-center">
                                            <Button variant="primary" type="submit">
                                                Submit
                                                </Button>
                                        </Form.Group>
                                        {this.ErrorMessage()}
                                    </Form>
                                :
                                    <Question questionList={this.state.questions} test={this.state.selectedTest} userName={this.state.name} />
                            }
                        </div>
                    </Row>
                </Container>
            </React.Fragment>
        );
    }
}