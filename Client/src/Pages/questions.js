// Dependencies
import { 
    Row,
    Form,
    Button,
    Col,
    ProgressBar,
    Alert
} from 'react-bootstrap';
import React, { Component } from 'react';
import axios from 'axios';

// Configuration file
import config from '../config';
// Local CSS
import './question.css';

export default class Landing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            questions: this.props.questionList,
            currentQuestion: 0,
            isNextDisabled: true,
            submitedAnswers: [],
            errorMessage: '',
            selectedTest: this.props.test,
            testResult: '',
            progress: 0,
            selectedAnswer: '',
            userName: this.props.userName
        };
    };

    answerSelection = (e) => {
        this.setState({
            progress: this.state.currentQuestion + 1,
            isNextDisabled: false,
            selectedAnswer: e.target.value
        });
    };

    answerSubmit = () => {
        const submitedAnswers = this.state.submitedAnswers;

        submitedAnswers.push({
            questionId: this.state.questions[this.state.currentQuestion].id,
            ans: this.state.selectedAnswer
        });

        this.setState({
            submitedAnswers: submitedAnswers,
        });

        if (this.state.submitedAnswers.length !== (this.state.currentQuestion + 1)) {
            this.setState({
                errorMessage: 'Please select at least one option'
            });
        } else if (this.state.submitedAnswers.length === this.state.questions.length) {
            axios
            .post(`${config.apiBaseURL}/result/${this.state.selectedTest}`, {
                submitedAnswer: this.state.submitedAnswers
            })
                .then((response) => {
                    this.setState({
                        testResult: response.data.message
                    })
                })
                .catch((error) => {
                    this.setState({
                        errorMessage: error.response && error.response.data && error.response.data.message ? error.response.data.message : 'Something went wrong.'
                    });
                })
        } else {
            this.setState({
                currentQuestion: this.state.currentQuestion + 1,
                isNextDisabled: true
            });
        }
    }

    ErrorMessage = () => {
        if (this.state.errorMessage !== "") {
            return (<Alert variant="danger">{this.state.errorMessage}</Alert>)
        }
    };

    render() {
        return (
            <div>
                {
                    this.state.testResult === '' ?
                        <Form onSubmit={this.submitForm}>
                            <Form.Group controlId="exampleForm.ControlSelect1">
                                <Form.Label>{this.state.questions[this.state.currentQuestion].question}</Form.Label>
                                <Row>
                                    {
                                        this.state.questions[this.state.currentQuestion].options.map((key, index) => {
                                            return (
                                                <Col md="6" key={index + key}>
                                                    <Form.Check
                                                        type="radio"
                                                        label={key}
                                                        name="option"
                                                        value={key}
                                                        onChange={this.answerSelection}
                                                        id={index + key}
                                                    />
                                                </Col>
                                            )
                                        })
                                    }
                                </Row>
                            </Form.Group>
                            <Button onClick={this.answerSubmit} variant="primary" disabled={this.state.isNextDisabled}>
                                {(this.state.currentQuestion + 1) === this.state.questions.length ? "Submit" : "Next"}
                            </Button>
                            <div className="user-alert">
                                <ProgressBar animated max={this.state.questions.length} variant="success" now={this.state.progress} />
                                <h6>You are going attempt {this.state.currentQuestion + 1} out of {this.state.questions.length}</h6>
                            </div>
                        </Form>
                    :
                        <h3>
                            Hi {this.state.userName},
                            <br />
                            {this.state.testResult}
                        </h3>
                }

                {this.ErrorMessage()}
            </div>
        )
    }
}