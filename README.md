# Printful-test

Follow steps to setup frontend and backend

#### Prerequisites:-
* Node.js v8.0.0
* NPM v5.6.0
* mysql-server 5.7.*

#### How to run ?
* Clone the repo by
```
git clone https://gitlab.com/AkibDeraiya123/printful-test.git
```
* Goto project folder by 
```
cd printful-test
```
* Create `.env`   at   `./Server`.(Take the reference from `./Server/example.env`).
* Create a database.
* Run migration script by (Make sure sql-server is running).
```
 cd ./Server && ./migrate.sh
```

* Back to root directory by
 ```
    cd ..
```

* Install dependences by (This command need to run from the root directory. It will install frontend and backend dependencies)
```
npm run install-dependencies
```

* Run backend and frontend by a single command(This command need to run From root directory)
```
npm start
```

* Server should start on port 3001. (Can be changed by `PORT` environment variable or change in `.env`. When you change this you need to change `apiBaseURL`  in  `./Client/src/config.js`)

#### API documentation.
* Attached postman collection into `./Server/postman-collection/printful-test.postman_collection.json`

#### Database structure
* Attached individual table schema into `./Server/database/`

#### Database Dump
* Attached database whole dump into `./Server/database-dump/`

