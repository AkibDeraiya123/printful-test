// Controller
const TestController = require('../controllers/TestController');

const routes = (app) => {
  // Get country list
  app.get(`${process.env.API_PREFIX}/test`, TestController.getTestList);
};

module.exports = { routes };
