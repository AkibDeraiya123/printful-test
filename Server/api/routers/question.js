// Controller
const QuestionController = require('../controllers/QuestionController');
// Validations
const QuestionValidations = require('../validations/QuestionValidations');

const routes = (app) => {
  // Get question list
  app.get(`${process.env.API_PREFIX}/questions/:test_id`, QuestionController.getQuestionList);
  // Check for result
  app.post(`${process.env.API_PREFIX}/result/:test_id`, QuestionValidations.checkResult, QuestionController.checkResult);
};

module.exports = { routes };
