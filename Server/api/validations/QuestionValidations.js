const Joi = require('joi');

module.exports.checkResult = (req, res, next) => {
    const schema = Joi.object().keys({
        submitedAnswer: Joi.array().items(
            Joi.object().keys({
                ans: Joi.alternatives(Joi.string(),Joi.number()),
                questionId: Joi.number()
            })
        )
      });
      
      Joi.validate(req.body, schema, {
        convert: true,
      }, (err) => {
        if (err) {
          return res.status(400).json({
            message: err.details[0].message
          });
        }
        next();
      });
};