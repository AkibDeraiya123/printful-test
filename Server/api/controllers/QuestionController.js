// models
const CommonQueryModel = require('../models/commonQueryModel');

// Service
const QuestionService = require('../services/QuestionService');

// Initial signup with basic user details
module.exports.getQuestionList = (req, res) => CommonQueryModel.selectRecord(
  ['id, question, options'],
  [
    {
      key: 'test_id',
      value: req.params.test_id,
    },
  ],
  'question'
)
  .then((questions) => {
    if (questions && questions.length) {
      questions.forEach(element => {
        element['options'] = JSON.parse(element.options);
      });

      return res.status(200).json({
        message: 'Question fetch successfully',
        result: {
          questions
        }
      });
    } else {
      return res.status(400).json({
        message: "Invalid test"
      })
    }
  })
  .catch(error => res.status(error.status || 500).json({
    message: error.message || 'Internal server error',
  }));

module.exports.checkResult = (req, res) => CommonQueryModel.selectRecord(
  ['id, correct_answer'],
  [
    {
      key: 'test_id',
      value: req.params.test_id,
    },
  ],
  'question'
)
  .then((questions) => {
    if (questions && questions.length) {
      if (req.body.submitedAnswer.length === questions.length) {
        return QuestionService.checkForResult(req.body.submitedAnswer, questions);
      } else if (req.body.submitedAnswer.length > questions.length) {
        throw ({
          status: 400,
          message: "Invalid request"
        });
      } else {
        throw ({
          status: 400,
          message: "Please attempt all the questions"
        });
      }
    } else {
      throw ({
        status: 400,
        message: "Invalid test"
      });
    }
  })
  .then(response => res.status(200).json({
    message: response
  }))
  .catch(error => res.status(error.status || 500).json({
    message: error.message || 'Internal server error',
  }));
