// models
const CommonQueryModel = require('../models/commonQueryModel');

// Get test list
module.exports.getTestList = (req, res) => CommonQueryModel.selectRecord(
  ['*'],
  [],
  'test_name'
)
  .then(tests => res.status(200).json({ message: 'Test fetch successfully', result: { test: tests } }))
  .catch(error => res.status(error.status || 500).json({ message: error.message || 'Internal server error' }));