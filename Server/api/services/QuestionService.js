const _ = require('lodash');

module.exports.checkForResult = (subutedAns, originalAns)  => {
    let result = 0;
    subutedAns.forEach((singleAns) => {
        const currectAnsObj = _.find(originalAns, ['id', singleAns.questionId]);

        if (currectAnsObj && currectAnsObj.correct_answer && currectAnsObj.correct_answer === singleAns.ans) {
            result++;
        }
    });

    return `You respond correclty ${result} out of ${originalAns.length}`;
};