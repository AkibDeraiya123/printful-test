CREATE TABLE `question` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `test_id` int(11) NOT NULL,
    `question` text,
    `options` json DEFAULT NULL,
    `correct_answer` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE question ADD foreign key (test_id) references test_name (id) on delete cascade on update cascade;


LOCK TABLES `question` WRITE;
INSERT INTO `question` VALUES
    ('1', '1', 'What does PHP stand for?', '[\"Personal Home Page\", \"Hypertext Preprocessor\", \"Pretext Hypertext Processor\", \"Preprocessor Home Page\"]', 'Hypertext Preprocessor'),
    ('2', '1', 'What is the default file extention for PHP files?', '[".html",".xml",".php",".ph"]', '.php'),
    ('3', '1', 'Which version of PHP introduced Try/catch Exception?', '["PHP 4","PHP 5","PHP 5.3","PHP 6"]', 'PHP 5'),
    ('4', '1', '_____use for multiline comment in PHP', '["//?","/* */","[*","<!-- --!>"]', '/* */'),

    ('5', '2', 'isNaN returns ....', '["Boolean", "Object", "Number", "Array"]', 'Boolean'),
    ('6', '2', '=== operator used for', '["Checking both operands have same value and but different type","Checking both operands have different value but same type","Checking both operands have different value and type","Checking both operands have same value and type"]', 'Checking both operands have same value and type'),
    ('7', '2', 'What would be the result of 3+2+"7"?', '["327","57","12","Throw error"]', '57'),
    ('8', '2', 'Use for single line comment', '["//?","/*","[*","//"]', '//'),

    ('9', '3', 'Which SQL keyword is used to retrieve a maximum value ?', '["MOST", "MAX","TOP","UPPER"]', 'MAX'),
    ('10', '3', 'Which of the following SQL clauses is used to DELETE tuples from a database table ?', '["DELETE", "REMOVE", "DROP", "CLEAR"]', 'DELETE'),
    ('11', '3', '___________removes all rows from a table without logging the individual row deletions.', '["DELETE", "REMOVE", "TRUNCATE", "CLEAR"]', 'TRUNCATE'),
    ('12', '3', '____use for comment in sql query', '["--","/*","[*","//"]', '--');
UNLOCK TABLES;