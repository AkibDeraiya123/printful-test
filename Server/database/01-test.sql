CREATE TABLE `test_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `test_name` WRITE;
INSERT INTO `test_name` VALUES
  (1,'PHP'),
  (2,'JavaScript'),
  (3,'SQL');
UNLOCK TABLES;