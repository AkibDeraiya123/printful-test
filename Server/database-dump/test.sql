-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: printful-test
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `question` text,
  `options` json DEFAULT NULL,
  `correct_answer` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `test_id` (`test_id`),
  CONSTRAINT `question_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `test_name` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,1,'What does PHP stand for?','[\"Personal Home Page\", \"Hypertext Preprocessor\", \"Pretext Hypertext Processor\", \"Preprocessor Home Page\"]','Hypertext Preprocessor'),(2,1,'What is the default file extention for PHP files?','[\".html\", \".xml\", \".php\", \".ph\"]','.php'),(3,1,'Which version of PHP introduced Try/catch Exception?','[\"PHP 4\", \"PHP 5\", \"PHP 5.3\", \"PHP 6\"]','PHP 5'),(4,1,'_____use for multiline comment in PHP','[\"//?\", \"/* */\", \"[*\", \"<!-- --!>\"]','/* */'),(5,2,'isNaN returns ....','[\"Boolean\", \"Object\", \"Number\", \"Array\"]','Boolean'),(6,2,'=== operator used for','[\"Checking both operands have same value and but different type\", \"Checking both operands have different value but same type\", \"Checking both operands have different value and type\", \"Checking both operands have same value and type\"]','Checking both operands have same value and type'),(7,2,'What would be the result of 3+2+\"7\"?','[\"327\", \"57\", \"12\", \"Throw error\"]','57'),(8,2,'Use for single line comment','[\"//?\", \"/*\", \"[*\", \"//\"]','//'),(9,3,'Which SQL keyword is used to retrieve a maximum value ?','[\"MOST\", \"MAX\", \"TOP\", \"UPPER\"]','MAX'),(10,3,'Which of the following SQL clauses is used to DELETE tuples from a database table ?','[\"DELETE\", \"REMOVE\", \"DROP\", \"CLEAR\"]','DELETE'),(11,3,'___________removes all rows from a table without logging the individual row deletions.','[\"DELETE\", \"REMOVE\", \"TRUNCATE\", \"CLEAR\"]','TRUNCATE'),(12,3,'____use for comment in sql query','[\"--\", \"/*\", \"[*\", \"//\"]','--');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_name`
--

DROP TABLE IF EXISTS `test_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_name`
--

LOCK TABLES `test_name` WRITE;
/*!40000 ALTER TABLE `test_name` DISABLE KEYS */;
INSERT INTO `test_name` VALUES (1,'PHP'),(2,'JavaScript'),(3,'SQL');
/*!40000 ALTER TABLE `test_name` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-03  1:38:27
