const chalk = require('chalk');
const express = require('express');
const bodyParser = require('body-parser');
const glob = require('glob');

// Load environment variables.
require('dotenv').config();

const http = require('http');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));
app.set('view engine', 'pug');

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-authorization');
  next();
});

const initRoutes = (app) => {
  // including all routes
  glob('./api/routers/*.js', (err, routes) => {
    if (err) {
      console.log('Error occured including routes');
      return;
    }
    routes.forEach((routePath) => {
      require(routePath).routes(app); // eslint-disable-line
    });
    console.warn('No of routes file : ', routes.length);
  });
};

initRoutes(app);

const httpServer = http.createServer(app);
httpServer.listen(process.env.PORT || 3000, () => {
  console.log(chalk.blue(`Printful-test server listening on port ${process.env.PORT}!`));
})